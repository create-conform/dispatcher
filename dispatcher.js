/////////////////////////////////////////////////////////////////////////////////////////////
//
// Dispatcher
//
//    Module for dispatching jobs to processing nodes.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
var path =       require("path");
var uuidv1 =     require("uuid/v1");
var event =      require("event");
var error =      require("error");
var host =       require("host");
var type =       require("type");
var handlebars = require("handlebars");
var sio =        require("socket.io-client");
var mongo =      require("mongodb");
var merge =      require("object-merge");

var Job =        require("job");
var Task =       require("task");

///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Extensions
//
///////////////////////////////////////////////////////////////////////////////////////////// 
Date.prototype.toHumanString = function() {
    function pad(number) {
        if (number < 10) {
            return "0" + number;
        }
        return number;
    }

    return this.getFullYear() + "-" + pad(this.getMonth()+1) + "-" + pad(this.getDate()) + " " + pad(this.getHours()) + ":" + pad(this.getMinutes()) + ":" + pad(this.getSeconds());
}

///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Global Handlebars Helpers
//
///////////////////////////////////////////////////////////////////////////////////////////// 
function hbsFQDN(dn) {
    return dn.split(":")[0];
}
function hbsPathWithoutfileExtension(filePath) {
    return path.win32.join(path.dirname(filePath), path.basename(filePath, path.extname(filePath)));
}
function hbsPathWithoutFilename(filePath) {
    return path.win32.dirname(filePath);
}
function hbsFilename(filePath) {
    return path.win32.basename(filePath);
}
function hbsFileExtension(filePath) {
    return path.win32.extname(filePath);
}
function hbsFilenameWithoutExtension(filePath) {
    return path.win32.basename(filePath, path.extname(filePath));
}
function hbsWindowsToUnix(filePath) {
    return filePath.replace(/\\/g, "/");
}
function hbsUnixToWindows(filePath) {
    return filePath.replace(/\//g, "\\");
}
function hbsUUID() {
    return uuidv1();
}

///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Dispatcher Class
//
///////////////////////////////////////////////////////////////////////////////////////////// 
function Dispatcher(db, options) {
    var self = this;

    var nodes = {};
    var connections = {};
    var services = {};
        options = options || {};
        options.db = options.db || {};
        options.db.collections = options.db.collections || {};
        options.db.collections.jobs = options.db.collections.jobs || this.DB_COLLECTION_JOBS;
        options.nodes = options.nodes || {};
        options.scheduler = options.scheduler || {};
        options.templates = options.templates || {};

    var events = new event.Emitter(this);
    var hbs = handlebars.create();
    var carouselTimeout;
    var destroyed;

    //
    // Public Functions
    //
    this.options = options;
    this.node = {
        get : function(address) {
            if (!address) {
                return nodes;
            }
            else {
                return nodes[address];
            }
        },
        add: function(address, options) {
            if (options.disabled) {
                return;
            }
            // connect to node, but do not add to nodes container, we need to wait for the register command.
            if (connections[address]) {
                throw new Error(this.ERROR_NODE_ALREADY_ADDED, "A connection to node '" + address + "' already exists.");
            }

            //TODO
            //safety check: forbid adding namespaces, or automatically remove them.

            var originalAddress = address;
            var port = address.split(":")[1] || 8181;
            var address = n.split(":")[0] + ":" + port;

            connections[address] = sio.connect("ws://" + address, { "autoConnect" : false });
            connections[address].on("register",   function(node) { 
                nodeRegister(originalAddress, address, node);
            });
            connections[address].on("unregister", function(node) { 
                nodeUnregister(originalAddress, address, node);
            });
            connections[address].on("busy",       function(server) {
                nodeBusy(address, server);
            });
            connections[address].on("disconnect", function() {
                nodeDisconnect(address);
            });
            connections[address].on("reconnect", function() {
                nodeReconnect(address);
            });
            connections[address].on("connect",    function() {
                nodeConnect(address);
            });
            connections[address].open();
        },
        remove: function(node) {
            // allow both just passing a string (which will be the name) or an object
        },
        disable: function(n) {
            // allow both just passing a string (which will be the name) or an object
        },
        enable: function(n) {
            // allow both just passing a string (which will be the name) or an object
        }
    };
    this.queueJob = function(job, /* optional */ parameters) {
        return new Promise(function(resolve, reject) {
            // process parameters
            if (type.isString(job)) {
                if (!options.templates.job[job]) {
                    throw new Error(self.ERROR_INVALID_TEMPLATE, "Job template '" + job + "' does not exist.");
                }

                job = options.templates.job[job];
            }

            if (job) {
                // get task templates
                if (job.tasks) {
                    for (var t=0;t<job.tasks.length;t++) {
                        if (type.isString(job.tasks[t].template)) {
                            job.tasks[t].template = options.templates.task[job.tasks[t].template];
                        }
                    }
                }

                // merge job parameters
                if (parameters) {
                    job.parameters = merge(parameters || {}, job.parameters || {});
                }
            }

            // validate
            job = new Job(job);

            db.collection(options.db.collections.jobs).insertOne(job, function(err, result) {
                if (err) return reject(err);
    
                job._id = result.insertedId;
    
                resolve(job);
    
                assignJobs();
            });
        });
    };
    this.destroy = function() {
        destroyed = true;

        clearTimeout(carouselTimeout);

        for (var s in services) {
            services[s].socket.close();
        }

        for (var c in connections) {
            connections[c].close();
        }

        for (var k in self) {
            delete self[k];
        }
    };

    //
    // Private Functions
    //
    function carousel() {
        if (destroyed) return;

        // clear timeout in case the carousel is invoked manually.
        clearTimeout(carouselTimeout);

        try {
            assignJobs();
        }
        catch(e) {
            console.error("[ SRV ]                         >> An error occured while trying to assign jobs.",e);
        }

        scheduleCarousel();
    }
    function scheduleCarousel() {
        if (destroyed) return;

        carouselTimeout = setTimeout(carousel, options.scheduler.interval || 60000);
    }
    function assignJobs() {
        for (var s in services) {
            nodeServiceAssign(s);
        }
    }
    function nodeConnect(address) {
        //DEBUG
        if (options.debug) console.log("[ SRV ] -->          CONNECT    >> node '" + address + "'");
    }
    function nodeDisconnect(address) {
        //DEBUG
        if (options.debug) console.log("[ SRV ] <--          DISCONNECT >> Node '" + address + "'");
        
        if (!nodes[address]) {
            return;
        }

        while (nodes[address].services.length > 0) {
            nodeServiceDisconnect(nodes[address].services[0].address);

            nodes[address].services.splice(0,1);
        }

        delete nodes[address];
    }
    function nodeReconnect(address) {
        //DEBUG
        if (options.debug) console.log("[ SRV ] -->          RECONNECT  >> node '" + address + "'");
    }
    function nodeResetJobs(svcAddress, excludeJobs) {
        return new Promise(function(resolve, reject) {
            var qry = {
                "tasks" : { 
                    "$elemMatch" : { 
                        "node" : svcAddress,
                        "started" : { "$ne" : null },
                        "ended" : null
                    }
                }
            };

            // create job exclusion list for the jobs that are still running after a reconnect
            var qryJobs = [];
            for (var j in excludeJobs) {
                if (!excludeJobs[j]) continue;

                qryJobs.push(mongo.ObjectId(excludeJobs[j].job._id));
            }
            if (qryJobs.length) {
                qry["_id"] = { "$nin" : qryJobs };

                //DEBUG
                if (options.debug) console.log("[ SRV ] -->                     >> service '" + svcAddress + "' is processing " + qryJobs.length + " job(s).");
            }

            // reset unfinished jobs
            db.collection(options.db.collections.jobs).updateMany(qry, {
                "$set" : {
                    "tasks.$.node" : null,
                    "tasks.$.started" : null
                },
                "$push": {
                    "tasks.$.log" : (new Date()).toHumanString() + ": Reset job status because node '" + svcAddress + "' disconnected before finishing."
                }
            }, function(err, result) {
                if (err) return reject(err);

                if (result.modifiedCount > 0) {
                    //DEBUG
                    if (options.debug) console.log("[ SRV ]                         >> " + result.modifiedCount + " job(s) where reset because the node disconnected before finishing the job(s).");
                }

                return resolve(svcAddress);
            });
        });
    }
    function nodeRegister(id, address, node) {
        //DEBUG
        if (options.debug) console.log("[ SRV ] <--          REGISTER   >> node '" + address + "': Services, Host Information", node.host);

        nodes[address] = {
            "id" : id,
            "address" : address,
            "services": [],
            "host": host.parse(node.host)
        };

        for(var s in node.services) {
            var svcAddress = address + "/" + node.services[s].type;

            //if (services[svcAddress]) {
            //    //DEBUG
            //    if (options.debug) console.warn("[ SRV ]                         >> skipping service '" + node.services[s].type + "': duplicate entry.");
            //    continue;
            //}

            //DEBUG
            if (options.debug) console.log("[ SRV ]                         >> Connecting to '" + svcAddress + "'.");

            nodeResetJobs(svcAddress,  node.services[s].jobInstances).then(dod(address, svcAddress, node.services[s])).catch(console.error);
        }

        function dod(address, svcAddress, service) {
            return function() {
                nodeServiceRegister(address, svcAddress, service);
            }
        }
    }
    function nodeUnregister(id, address, node) {
        //DEBUG
        if (options.debug) console.log("[ SRV ] <--          UNREGISTER >> node '" + address + "'");
        
    }
    function nodeBusy(address, server) {
        //DEBUG
        if (options.debug) console.log("[ SRV ] <--          BUSY       >> node '" + address + "': This node is registered to server '" + server + "'.");
    }
    function nodeServiceRegister(address, svcAddress, service) {
        if (!services[svcAddress]) {
            services[svcAddress] = {
                "address" : svcAddress,
                "service" : service,
                "socket" : sio.connect("ws://" + svcAddress, { "autoConnect" : false }),
                "jobInstances" : service.jobInstances || [],
                "node" : nodes[address]
            }

            services[svcAddress].socket.on("job",        function(job) {
                if (job) {
                    nodeServiceUpdateJob(svcAddress, job);
                }
                else {
                    nodeServiceAssign(svcAddress);
                }
            });
            services[svcAddress].socket.on("reject",     function(feedback) {
                nodeServiceRejectJob(svcAddress, feedback);
            });
            services[svcAddress].socket.on("reconnect",     function() {
                if (options.debug) console.log("[ SRV ] <--          RECONNECT  >> service '" + svcAddress + "'.");
            });
            services[svcAddress].socket.open();
        }
        else {
            services[svcAddress].service = service;
            services[svcAddress].jobInstances = service.jobInstances || [];
            services[svcAddress].node = nodes[address];
        }

        delete service.jobInstances;

        nodes[address].services.push(services[svcAddress]);

        nodeServiceAssign(svcAddress);
    }
    function nodeServiceAssign(svcAddress) {
        for (var i=0; i<(services[svcAddress].service.maxInstances || 1); i++) {
            // skip if instance has job assigned or is disabled
            if (!services[svcAddress].socket.connected || services[svcAddress].node.disabled || services[svcAddress].disabled || services[svcAddress].jobInstances[i]) {
                continue;
            }

            nodeServiceAssignTask(svcAddress, i);
        }
    }
    function nodeServiceAssignTask(svcAddress, instance) {
        //DEBUG
        //if (options.debug) console.log("[ SRV ]                         >> Looking to assign a task to '" + svcAddress + "' (type: '" + services[svcAddress].service.type + "', instance: '" + instance + "').");

        // generate job start date
        var started = new Date();

        // get the templates that are configured to be supported by this node
        var templates = [];
        for (var t in options.nodes[services[svcAddress].node.id].templates) {
            templates.push(options.templates.task[options.nodes[services[svcAddress].node.id].templates[t]]);
        }

        // construct search query for available templates/jobs
        var elemMatch = {
            "node" : null,
            "ready" : true
        }
        if (templates.length) {
            elemMatch.template = {
                "$in" : templates
            }
        }
        else {
            elemMatch["template.service"] = services[svcAddress].service.type;
        }

        // load next job stack from database
        db.collection(options.db.collections.jobs).findOneAndUpdate({
            "$and" : [ { "$or" : [ { "scheduled" : { $lte: started } }, { "scheduled" : null } ] }, { "$or" : [ { "tasks.scheduled" : { $lte: started } }, { "tasks.scheduled" : null } ] } ],
            "tasks" : { 
                $elemMatch: elemMatch,
                $not : {
                    $elemMatch : {
                        "error" : null,
                        "started" : { "$ne" : null },
                        "ended" : null
                    }
                }
            }
        }, {
            "$set" : {
                "tasks.$.node" : svcAddress,
                "tasks.$.started" : started,
                "tasks.$.ended" : null
            },
            "$push": {
                "tasks.$.log" : started.toHumanString() + ": Task assigned to node '" + svcAddress + "'."
            }
        }, {
            "sort": {
                "scheduled": 1,
                "created" : 1
            },
            "returnOriginal" : false
        }).then(function(result) {
            if (result.value) {
                function processParameter(task, val) {
                    if (type.isString(val)) {
                        return hbs.compile(val)({ "job" : result.value, "task" : task });
                    }
                    if (type.isObject(val) || type.isArray(val)) {
                        for (var p in val) {
                            val[p] = processParameter(task, val[p]);
                        }
                    }
                    return val;
                }

                // tasks
                var task = null;

                // compile task with handlebars
                for (var t in result.value.tasks) {
                    if (result.value.tasks[t].ready && result.value.tasks[t].node == svcAddress && result.value.tasks[t].started.toISOString && result.value.tasks[t].started.toISOString() == started.toISOString() && !result.value.tasks[t].ended) {
                        task = result.value.tasks[t];

                        // add template parameters
                        if (result.value.tasks[t].template && result.value.tasks[t].template.parameters) {
                            result.value.tasks[t].parameters = result.value.tasks[t].parameters || {};
                            result.value.tasks[t].parameters = merge(result.value.tasks[t].template.parameters, result.value.tasks[t].parameters);
                        }

                        // process task parameters
                        for (var p in result.value.tasks[t].parameters) {
                            var val = processParameter(result.value.tasks[t], result.value.tasks[t].parameters[p]);
                            result.value.tasks[t].parameters[p] = val;
                        }
                        break;
                    }
                }

                //DEBUG
                if (options.debug) console.log("[ SRV ]                         >> Assign job '" + result.value._id + "', task: '" + t + "' to '" + svcAddress + "'.");

                services[svcAddress].jobInstances[instance] = { "job" : result.value, "taskIdx" : t };

                // send job to node
                services[svcAddress].socket.emit("job", services[svcAddress].jobInstances[instance] );
            }
            else {
                //DEBUG
                //if (options.debug) console.log("[ SRV ]                         >> No tasks for node '" + svcAddress + "'.");
            }
        }).catch(function() {
            //DEBUG
            console.error.apply(console, arguments);

            //TODO
            //Update job in db, set error flag to task
        });
    }
    function nodeServiceUpdateJob(svcAddress, job) {
        var ended = new Date();
        
        var closeTask = false;
      
        // mark task end dates
        var allTasksComplete = true;
        var nextReady = false;
        for (var t in job.tasks) {
            if (nextReady) {
                allTasksComplete = false;
                job.tasks[t].ready = true;
                break;
            }
            if (job.tasks[t].ended === true) {
                closeTask = true;
                job.tasks[t].ended = ended;
                job.tasks[t].log.push(ended.toHumanString() + ": Job finished on '" + svcAddress + "'.");
        
                if (!job.tasks[t].error) {
                    nextReady = true;
                }
            }
            else if (!job.tasks[t].ended) {
                allTasksComplete = false;
            }
        }
      
        // if all closed, update job end time
        var set = { "tasks" : job.tasks };
        if (allTasksComplete) {
            set["ended"] = ended;

            try {
                events.fire(self.EVENT_JOB_FINISHED, job);
            }
            catch(e) {
                console.error("[ SRV ] Error in event callback for event 'job-finished'. ", e);
            }
        }
      
        //DEBUG
        if (options.debug) console.log("[ SRV ]                         >> Update Job '" + job._id + "'.");

        db.collection(options.db.collections.jobs).update({
            "_id" : mongo.ObjectId(job._id) 
        }, {
            "$set" : set
        }).then(function() {
            if (closeTask) {
                // find the instance, and reset the job instance state
                for (var j in services[svcAddress].jobInstances) {
                    if (services[svcAddress].jobInstances[j].job._id == job._id) {

                        if (options.debug) console.log("[ SRV ]                         >> Closing job in slot '" + j + "'.");

                        services[svcAddress].jobInstances[j] = null;

                        // assign a new job to the free instance -> replaced by carousel
                        //nodeServiceAssignTask(svcAddress, j);

                        carousel();

                        break;
                    }
                }
            }
        }).catch(function(e) {
            //DEBUG
            if (options.debug) console.error("[ SRV ]                         >> Could not update job '" + job._id + "'. The render node will be stuck until you restart the dispatcher.", e);
        });
    }
    function nodeServiceRejectJob(svcAddress, feedback) {
        //DEBUG
        // feedback.job
        // feedback.reason
        // feedback.message (optional)
        if (options.debug) console.log("[ SRV ] <--          REJECT     >> node '" + svcAddress + "' rejected the job it was assigned. Reason: '" + feedback.reason + "'.");
    }
    function nodeServiceDisconnect(svcAddress) {
        // destroy socket -> does not work, when service is stopped and started quickly, a reconnectstill occurs
        //TODO - I WAS HERE: REIMPLEMENT -> REUSE THE "NODE" SOCKETS INSTEAD OF CREATING NEW ONES ON REGISTER
        //if (options.debug) console.log("[ SRV ] <--                     >> destroy '" + svcAddress + "'.");
        // services[svcAddress].socket.disconnect(true);

        // remove from carousel
        //delete services[svcAddress];

        // resetting does not apply anymore, resetting is done when a node registers again. this is because a netwerk hickup could
        // cause this, and the node could actually be fine. when it registers, it tells the dispatcher what it's currently doing.
        //nodeResetJobs(svcAddress);
    }

    //
    // Init
    //
    // add specific handlers
    hbs.registerHelper("isLinux", function(svcAddress, options) {
        if (services[svcAddress] && services[svcAddress].node.host.isPlatformLinuxFamily()) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    hbs.registerHelper("isWindows", function(svcAddress, options) {
        if (services[svcAddress] && services[svcAddress].node.host.platform == host.PLATFORM_WINDOWS) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    hbs.registerHelper("isMacOS", function(svcAddress, options) {
        if (services[svcAddress] && services[svcAddress].node.host.platform == host.PLATFORM_MACOS) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    // add global handlers
    hbs.registerHelper("fqdn", hbsFQDN);
    hbs.registerHelper("pathWithoutFileExtension", hbsPathWithoutfileExtension);
    hbs.registerHelper("pathWithoutFilename", hbsPathWithoutFilename);
    hbs.registerHelper("filename", hbsFilename);
    hbs.registerHelper("fileExtension", hbsFileExtension);
    hbs.registerHelper("filenameWithoutExtension", hbsFilenameWithoutExtension);
    hbs.registerHelper("windowsToUnix", hbsWindowsToUnix);
    hbs.registerHelper("unixToWindows", hbsUnixToWindows);
    hbs.registerHelper("hbsUUID", hbsUUID);

    // start by assigning tasks to the newly added nodes
    if (options && options.nodes) {
        for (var n in options.nodes) {
            this.node.add(n, options.nodes[n]);
        }
    }

    // start the carousel
    scheduleCarousel();
}
Dispatcher.prototype.DB_COLLECTION_JOBS = "jobs";
Dispatcher.prototype.ERROR_NODE_ALREADY_ADDED =    "Node Already Added";
Dispatcher.prototype.ERROR_INVALID_TEMPLATE =      "Invalid Template";
Dispatcher.prototype.EVENT_NODE_ADDED =    "node-added";
Dispatcher.prototype.EVENT_NODE_REMOVED =  "node-removed";
Dispatcher.prototype.EVENT_NODE_DISABLED = "node-disabled";
Dispatcher.prototype.EVENT_NODE_ENABLED =  "node-enabled";
Dispatcher.prototype.EVENT_JOB_FINISHED =  "job-finished";

///////////////////////////////////////////////////////////////////////////////////////////// 
module.exports = Dispatcher;